<?php
Class BannerWidget Extends SVGWidget {
	public function setMember(Member $member) {
		$this->member = $member;
	}

	public function getMember() {
		return $this->member;
	}

	public function getOutputPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.$this->member->id.'/'.Yii::app()->languages->currentLanguage.'/'.$this->scale.'.png';
	}

	protected function getTempPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.$this->member->id.'/'.Yii::app()->languages->currentLanguage.'.svg';
	}
}