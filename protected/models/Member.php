<?php
Class Member Extends CActiveRecord {
	protected $_passwordConfirmation;

	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function tableName() {
		return 'member';
	}

	public function rules() {
		return array(
			array('first_name, last_name, email, address, public', 'safe'),
			array('email', 'email'),

			array('email', 'required', 'on' => 'recover'),

			array('first_name, last_name, address, password, passwordConfirmation, email', 'required', 'on' => 'info'),
			array('number', 'safe', 'on' => 'update'),
			array('password', 'compare', 'on' => 'info', 'compareAttribute' => 'passwordConfirmation', 'message' => Yii::t('member', 'Password and its confirmation dont match')),
			array('email', 'unique', 'on' => 'info'),

			array('first_name, last_name, address, email', 'required', 'on' => 'profile'),
			array('password', 'compare', 'safe' => False, 'on' => 'profile', 'compareAttribute' => 'passwordConfirmation', 'allowEmpty' => True, 'message' => Yii::t('member', 'Password and its confirmation dont match')),
			array('email', 'unique', 'on' => 'profile'),

			array('email, password', 'required', 'on' => 'login'),
		);
	}

	public function attributeLabels() {
		$labels = array(
			'first_name' => Yii::t('member', 'First name'),
			'last_name' => Yii::t('member', 'Last name'),
			'email' => Yii::t('member', 'Email address'),
			'birth_date' => Yii::t('member', ' Birth Date'),
			'address' => Yii::t('member', 'Address'),
			'password' => Yii::t('member', 'Password'),
			'passwordConfirmation' => Yii::t('member', 'Password Confirmation'),
		);

		if(in_array($this->scenario, array('info', 'profile')))
			$labels['public'] = Yii::t('member', 'Do you want your name to be publicly mentioned?');

		return $labels;
	}

	public function getFullName() {
		return $this->first_name.' '.$this->last_name;
	}

	public function getPasswordConfirmation() {
		return $this->_passwordConfirmation;
	}

	public function setPasswordConfirmation($passwordConfirmation) {
		$this->_passwordConfirmation= $passwordConfirmation;
	}

	public function save($runValidations = True, $attributes=Null) {
		if($this->isNewRecord)
			$this->register_time = time();
		else {
			$this->update_time = time();
			$this->updater = Yii::app()->user->id;
		}

		return parent::save($runValidations, $attributes=Null);
	}

	public function relations() {
		return array(
			'updater_member' => array(self::BELONGS_TO, 'Member', 'updater')
		);
	}

	public function getIsAdmin() {
		return Yii::app()->authManager->checkAccess('admin', $this->id);
	}

	public function setIsAdmin($isAdmin) {
		if($isAdmin)
			if($this->isAdmin)
				return ;
			else
				Yii::app()->authManager->assign('admin', $this->id);
		else
			Yii::app()->authManager->revoke('admin', $this->id);

		Yii::app()->authManager->save();
	}

	public function getLastPayment() {
		$criteria = New CDbCriteria;
		$criteria->addColumnCondition(array('member'=>$this->id, 'status' => Payment::Done));
		$criteria->order = 'date DESC';

		return Payment::model()->find($criteria);
	}

	public function getFirstPayment() {
		$criteria = New CDbCriteria;
		$criteria->addColumnCondition(array('member'=>$this->id, 'status' => Payment::Done));
		$criteria->order = 'date ASC';

		return Payment::model()->find($criteria);
	}

	public function getNextPaymentDate() {
		$lastPayment = $this->lastPayment;
		if(!$lastPayment)
			return time(); //Has no payment. Pay NOW dude!

		$passedDays = (Int)((time() - $lastPayment->date)/86400);

		$dailyAverage = 100/365;
		$payedAmount = $lastPayment->amount;

		if($payedAmount <= $dailyAverage * $passedDays) //Dude is already late :(
			return time();

		$remainingAmount = $payedAmount - ($dailyAverage * $passedDays);

		return time() + ((Int)($remainingAmount / $dailyAverage)*86400);
	}

	public function getLastPaymentAmount() {
		return $this->lastPayment->amount;
	}
}