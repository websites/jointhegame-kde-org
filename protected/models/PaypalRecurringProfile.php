<?php
Class PaypalRecurringProfile Extends CActiveRecord {
	protected $_details;
	protected $_transactions;

	public static function model($model = __CLASS__) {
		return parent::model($model);
	}

	public function tableName() {
		return 'paypal_recurring_profile';
	}

	public function relations() {
		return array(
			'owner' => array(self::BELONGS_TO, 'Member', 'member')
		);
	}

	public function getDetails() {
		if($this->_details)
			return $this->_details;

		$paypal = New PaypalPayment;
		$this->_details = $paypal->getRecurringDetails($this);
		return $this->_details;
	}

	public function getTransactions() {
		if($this->_transactions)
			return $this->_transactions;

		$paypal = New PaypalPayment;
		$transactions = $paypal->findTransactionsByProfile($this);
		$this->_transactions = $transactions;
		return $transactions;
	}

	public function update() {
		$paypal = New PaypalPayment;
		$paypal->updateRecurring($this);
	}
}