<?php
Class PaypalPayment Extends Payment {
	protected $_isRecurring;
	protected $_token;
	protected $_redirectUrl;

	//Methods
	const SetExpressCheckout = 'SetExpressCheckout';
	const GetExpressCheckoutDetails = 'GetExpressCheckoutDetails';
	const DoExpressCheckoutPayment = 'DoExpressCheckoutPayment';
	const CreateRecurringPaymentsProfile = 'CreateRecurringPaymentsProfile';
	const GetRecurringPaymentsProfileDetails = 'GetRecurringPaymentsProfileDetails';
	const TransactionSearch = 'TransactionSearch';
	const GetTransactionDetails = 'GetTransactionDetails';

	// Request parameters
	const Method = 'METHOD';
	const Version = 'VERSION';
	const User = 'USER';
	const Password = 'PWD';
	const Signature = 'SIGNATURE';
	const Payer = 'PAYERID';
	const Action = 'PAYMENTACTION';
	const StartDate = 'PROFILESTARTDATE';
	const Description = 'L_BILLINGAGREEMENTDESCRIPTION0';
	const PeriodUnit = 'BILLINGPERIOD';
	const PeriodFrequency = 'BILLINGFREQUENCY';

	const Amount = 'AMT';
	const MaximumAmount = 'MAXAMT';
	const ReturnUrl = 'RETURNURL';
	const CancelUrl = 'CANCELURL';
	const Currency = 'CURRENCYCODE';

	const Euro = 'EUR';
	const Usd = 'USD';

	const Sale = 'Sale';
	const Authorization = 'Authorization';

	const Day = 'Day';

	//Response parameters
	const Token = 'TOKEN';
	const Time = 'TIMESTAMP';
	const Correlation = 'CORRELATIONID';
	const Ack = 'ACK';
	const Build = 'BUILD';
	const Status = 'PAYMENTSTATUS';
	const Transaction = 'TRANSACTIONID';
	const Profile = 'PROFILEID';


	//Response values
	const Success = 'Success';
	const Pending = 'Pending';
	const Completed = 'Completed';
	const Failed = 'Failed';


	public function getTitle() {
		return Yii::t('payment', 'Paypal');
	}

	public function pay($redirectUrl) {
		$this->redirectUrl = $redirectUrl;
		Yii::app()->session->add('paypal', $this);
		Yii::app()->controller->render('/payment/paypal/details', array('model' => $this));
	}

	public function start() {
		$request = array(
			self::ReturnUrl => Yii::app()->createAbsoluteUrl('/paypal/resume'),
			self::CancelUrl => Yii::app()->createAbsoluteUrl('/paypal/cancel'),
			self::Currency => self::Euro,
			self::Amount => $this->amount
		);

		if($this->isRecurring) {
			$request[self::MaximumAmount] = $this->amount;
			$request[self::Description] = $this->description;
			$request[self::Action] = self::Authorization;
			$request['L_BILLINGTYPE0'] = 'RecurringPayments';
			$request['L_PAYMENTTYPE0'] = 'Any';
		} else
			$request[self::Action] = self::Sale;

		$response = $this->request(self::SetExpressCheckout, $request);

		if($response[self::Ack] != self::Success)
			return False;

		$this->token = $response[self::Token];

		Yii::app()->request->redirect('https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token='.$this->token);
	}

	private function makeNvp($array) {
		$str = '';
		foreach($array as $name => $value)
			$str .= $name.'='.urlencode($value).'&';

		return $str;
	}

	private function request($method, Array $params) {
		$uri = 'https://api-3t.sandbox.paypal.com/nvp?';

		$params[self::Method] = $method;

		$params[self::Version] = '57.0';
		$params[self::User] = 'emilse_1295956867_biz_api1.gmail.com';
		$params[self::Password] = '1295956876';
		$params[self::Signature] = 'AtmPYroY2itnT.tMJgTAqPAWqWu2AA8yuLts0w.GPGFNVk.fXn--CvxN';

		$querystring = $this->makeNvp($params);

		$response = file_get_contents($uri.$querystring);
		$response = explode('&', $response);
		$array = Array();
		foreach($response as $part) {
			$part = explode('=', $part);
			$array[$part[0]] = urldecode($part[1]);
		}

		return $array;
	}

	public function updateDetails() {
		$response = $this->request(self::GetExpressCheckoutDetails, array(
			self::Token => $this->token
		));

		if($response[self::Ack] != self::Success)
			return False;

		$this->amount = (float)$response[self::Amount];
		$this->date = time();
		$this->status = $this->getProperStatus($response[self::Status]);
		$this->transaction = $response[self::Transaction];

		return $response;
	}

	public function saveAutomatically($runValidations = True) {
		$details = $this->updateDetails();

		$request = Array(
			self::Token => $this->token,
			self::Payer => $details[self::Payer],
			self::Currency => self::Euro,
			self::Amount => $this->amount
		);

		if($this->isRecurring)
			$request[self::Action] = self::Authorization;
		else
			$request[self::Action] = self::Sale;

		$response = $this->request(self::DoExpressCheckoutPayment, $request);


		if($response[self::Ack] != self::Success)
			return False;

		if(!$this->isRecurring)
			return parent::save($runValidations);

		$request = array(
			self::Token => $this->token,
			self::StartDate => date('c', time()),
			'DESC' => $this->description,
			self::PeriodUnit => self::Day,
			self::PeriodFrequency => $this->period,
			self::Amount => $this->amount,
			self::Currency => self::Euro
		);

		$response = $this->request(self::CreateRecurringPaymentsProfile, $request);

		if($response[self::Ack] != self::Success)
			return False;

		$profile = New PaypalRecurringProfile;
		$profile->profile = $response[self::Profile];
		$profile->member = $this->member;
		$profile->save();
		$this->updateRecurring($profile);
		return True;
	}

	public function setIsRecurring($isRecurring) {
		$this->_isRecurring = $isRecurring;
	}

	public function getIsRecurring() {
		return $this->_isRecurring;
	}

	public function setToken($token) {
		$this->_token = $token;
	}

	public function getToken() {
		return $this->_token;
	}

	public function setRedirectUrl($redirectUrl) {
		$this->_redirectUrl = $redirectUrl;
	}

	public function getRedirectUrl() {
		return $this->_redirectUrl;
	}

	public function getDescription() {
		return "{$this->amount} EUR every {$this->period} days";
	}

	public function getRecurringDetails(PaypalRecurringProfile $profile) {
		$response = $this->request(self::GetRecurringPaymentsProfileDetails, Array(
			self::Profile => urldecode($profile->profile)
		));
		return $response;
	}

	public function search($params) {
		$response = $this->request(self::TransactionSearch, $params);

		$results = Array();
		$i = 0;
		while(isset($response['L_TRANSACTIONID'.$i])) {
			$results[] = $response['L_TRANSACTIONID'.$i];
			$i++;
		}
		return $results;
	}

	public function findTransactionsByProfile(PaypalRecurringProfile $profile) {
		$transactions = $this->search(array(
			self::Profile => $profile->profile,
			'STARTDATE' => $profile->details[self::StartDate]
		));
		array_shift($transactions);
		return $transactions;
	}

	public function updateRecurring(PaypalRecurringProfile $profile) {
		$transactions = $this->findTransactionsByProfile($profile);

		foreach($transactions as $transactionId) {
			$transaction = $this->request(self::GetTransactionDetails, array(self::Transaction => $transactionId));
			if($transaction[self::Ack] !== self::Success)
				continue;

			$payment = PaypalPayment::model()->findByAttributes(array('transaction' => $transactionId));
			if(!$payment)
				$payment = New PaypalPayment;

			$payment->member = $profile->member;
			$payment->transaction = $transactionId;
			$payment->status = $this->getProperStatus($transaction[self::Status]);
			$payment->amount = $transaction[self::Amount];
			$payment->date = strtotime($transaction['ORDERTIME']);
			$payment->save();
		}
	}

	protected function getProperStatus($status) {
		Switch ($status) {
			case self::Completed:
			case Null:
				return self::Done;
			break;

			case self::Pending:
				return self::Pending;
			break;

			case self::Failed:
				return self::Failed;
			break;
		}
	}

	public function getCouldAutomaticallyUpdate() {
		return (Boolean) $this->transaction;
	}

	public function automaticUpdate() {
		$transaction = $this->request(self::GetTransactionDetails, array(self::Transaction => $this->transaction));

		if($transaction[self::Ack] !== self::Success)
			return ;

		$this->status = $this->getProperStatus($transaction[self::Status]);
		$this->amount = $transaction[self::Amount];
		$this->date = strtotime($transaction['ORDERTIME']);
		return $this->save();
	}
}