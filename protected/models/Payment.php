<?php
Class Payment Extends CActiveRecord {
	const ANNUALY = 'annualy';
	const QUARTERLY = 'quarterly';

	const Done = 'Done';
	const Pending = 'Pending';
	const Failed = 'Failed';

	protected $_period;

	public static function model($method = __CLASS__) {
		if($method == __CLASS__)
			return parent::model(__CLASS__);
		else
			return parent::model($method.'Payment');
	}

	public function init() {
		$this->method = $this->getMethod();
	}

	public function tableName() {
		return 'payment';
	}

	public function rules() {
		return Array(
			Array('member, amount, date', 'required', 'on' => 'create'),
			Array('member, amount, status, method', 'safe', 'on' => 'create'),
		);
	}

	public static function getMethods() {
		return Array(New StandardPaypalPayment, New DirectTransferPayment, New DirectDebitAuthorizationPayment);
	}

	public static function getPeriods() {
		return array(
			self::ANNUALY => Yii::t('payment', 'Annualy'),
			self::QUARTERLY => Yii::t('payment', 'Quarterly'),
		);
	}

	public function getMethod() {
		return str_replace('Payment', Null, get_class($this)); // if class name if FooPayment, return Foo.
	}

	public function getStatuses() {
		return Array(
			self::Done => Yii::t('payment', 'Done'),
			self::Pending => Yii::t('payment', 'Pending'),
			self::Failed => Yii::t('payment', 'Failed')
		);
	}

	public function pay($redirectUrl) {
		return False;
	}

	public function getTitle() {
		return False;
	}

	public function relations() {
		return array(
			'owner' => array(self::BELONGS_TO, 'Member', 'member')
		);
	}

	public function instantiate($attributes) {
		$method = $attributes['method'];
		$name = $method.'Payment';
		return New $name(null);
	}

	public function getFormattedDate() {
		if($this->date)
			return Yii::app()->format->formatDate($this->date);
	}

	public function setPeriod($period) {
		$this->_period = (Int)$period;
	}

	public function getPeriod() {
		return $this->_period;
	}

	public function getCouldAutomaticallyUpdate() {
		return False;
	}

	public function getMethodTitle() {
		foreach(self::getMethods() as $method)
			if($method->getMethod() == $this->getMethod())
				return $method->title;
	}

	public function save($runValidations = True, $attributes=Null) {
		if(!$this->status)
			$this->status = self::Done;

		if(!$this->method)
			$this->setAttribute('method', $this->getMethod());
		return parent::save($runValidations, $attributes);
	}
}

Class DirectDebitAuthorizationPayment Extends Payment {
	public function getTitle() {
		return Yii::t('payment', 'Direct Debit Authorization (Germany only)');
	}

	public function pay($redirectUrl) {
		Yii::import('ext.yii-mail.*');
		$mail = New YiiMailMessage;
		$mail->subject = Yii::t('join', 'Your payment for Join The Game campaign');
		$mail->from = 'jointhegame-admin@kde.org';
		$mail->to = array($this->owner->email);
		$mail->body = Yii::app()->controller->renderPartial('/mail/direct_debit', array('model' => $this->owner), True, True);
		Yii::app()->mail->send($mail);

		$this->status = self::Pending;
		$this->save();
		Yii::app()->controller->redirect($redirectUrl);
	}
}

Class DirectTransferPayment Extends Payment {
	public function getTitle() {
		return Yii::t('payment', 'Direct Transfer (Manual)');
	}

	public function pay($redirectUrl) {
		Yii::import('ext.yii-mail.*');
		$mail = New YiiMailMessage;
		$mail->subject = Yii::t('join', 'Your payment for Join The Game campaign');
		$mail->from = 'jointhegame-admin@kde.org';
		$mail->to = array($this->owner->email);
		$mail->body = Yii::app()->controller->renderPartial('/mail/direct_transfer', array('model' => $this->owner), True, True);
		Yii::app()->mail->send($mail);

		$this->status = self::Pending;
		$this->save();
		Yii::app()->controller->redirect($redirectUrl);
	}
}

Class ManualPaypalPayment Extends Payment {
	public function getTitle() {
		return Yii::t('payment', 'Paypal (manual)');
	}

	public function pay($redirectUrl) {
		Yii::import('ext.yii-mail.*');
		$mail = New YiiMailMessage;
		$mail->subject = Yii::t('join', 'Your payment for Join The Game campaign');
		$mail->from = 'jointhegame-admin@kde.org';
		$mail->to = array($this->owner->email);
		$mail->body = Yii::app()->controller->renderPartial('/mail/paypal_manual', array('model' => $this->owner), True, True);
		Yii::app()->mail->send($mail);

		$this->status = self::Pending;
		$this->save();
		Yii::app()->controller->redirect($redirectUrl);
	}
}

Class StandardPaypalPayment Extends Payment {
	const Verified = 'VERIFIED';
	const Completed = 'Completed';

	public function getTitle() {
		return Yii::t('payment', 'Paypal (instant, recommended)');
	}

	public function pay($redirectUrl) {
		Yii::app()->controller->render('/payment/paypal/standard/button', array(
			'model' => $this,
			'redirectUrl' => $redirectUrl
		));
	}

	public function saveAutomatically($data) {
		if($data['txn_type'] != 'web_accept')
			return False;

		$data['cmd'] = '_notify-validate';

		$config=new CConfiguration(Yii::getPathOfAlias('application.config').'/paypal.php');
		$curl = curl_init( $config['url'] );

		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, True );
		curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, True );
		curl_setopt( $curl, CURLOPT_POST, True );
		curl_setopt( $curl, CURLOPT_POSTFIELDS, $data );

		$response = curl_exec( $curl );

		$this->transaction = $data['txn_id'];
		$this->member = $data['custom'];
		$this->date = strtotime($data['payment_date']);
		$this->amount = $data['mc_gross'];

		if($response == self::Verified) {
			Switch ($data['payment_status']) {
				case self::Completed:
					$this->status = self::Done;
				break;

				case self::Pending:
					$this->status = self::Pending;
				break;

				case self::Failed:
					$this->status = self::Failed;
				break;
			}
		} else
			$this->status = self::Failed;

		$this->save(False);
	}
}