<?php $form = $this->beginWidget('CActiveForm'); ?>
	<?php $this->extra = CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<br />
		<?php echo $form->textField($model, 'email'); ?>
		<br />
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(); ?>
	</div>
<?php $this->endWidget(); ?>