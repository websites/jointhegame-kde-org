<?php
if($this->teaser) {
	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.nivo.slider.pack.js');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/nivo-slider.css');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl;?>/css/style.css" />
		<title><?php echo Yii::t('theme', 'Join The Game'); ?></title>

		<?php if($this->teaser) { ?>
			<script type="text/javascript">
				$(window).load(function() {
					$('#teaser').nivoSlider({
						directionNav: false,
						controlNav:false,
						keyboardNav:false,
						effect:'sliceDown',
						pauseTime:10000
					});
				});

				$(document).ready(function() {
					$('#language').hover(function() {
						$(this).hide();
						$('#language_selector').show();
					});

					var preventRevert= false;
					$('#language_selector').blur(function() {
						preventRevert = false;
						$(this).hide();
						$('#language').show();
					});

					$('#language_selector').focus(function() {
						preventRevert = true;
					});

					$('#language_selector').mouseout(function() {
						if(preventRevert)
							return ;

						$(this).hide();
						$('#language').show();
					});

					$('#language_selector').change(function() {
						$(this).hide();
						$('#language').html($(this).find(':selected').html()).show();
						window.location = '<?php echo Yii::app()->createUrl('/language/change');?>/'+$(this).val();
					});
				});
			</script>
		<?php } ?>
	</head>
	<body>
		<div id="top">
			<?php if($this->teaser) { ?>
				<div id="teaser_container">
					<div id="main_text">
						<?php echo Yii::t('join', 'Show your love for KDE, join the game!'); ?>
					</div>
					<div id="teaser_background">
						<div style="margin-top: 30px;margin-left: 30px;">
							<div id="teaser">
								<img src="<?php echo Yii::app()->baseUrl.'/css/images/teaser/software.png'; ?>" alt="" />
								<img src="<?php echo Yii::app()->baseUrl.'/css/images/teaser/people.png'; ?>" style="display: none;" alt="" />
								<img src="<?php echo Yii::app()->baseUrl.'/css/images/teaser/freedom.png'; ?>" style="display: none;"  alt="" />
								<?php if($this->showJoinStuff) { ?>
									<img src="<?php echo Yii::app()->baseUrl.'/css/images/teaser/join.png'; ?>" style="display: none;" alt="" />
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>

		<div id="info">
			<div id="info_container">
				<div id="title">
					<?php echo $this->title ?>
				</div>

				<?php if($this->extra) { ?>
					<div id="extra">
						<?php echo $this->extra; ?>
					</div>
				<?php } ?>
			</div>
		</div>

		<div id="content">
			<?php echo $content; ?>
		</div>

		<div id="footer">
			<div id="menu_container">
				<ul id="menu">
					<?php foreach($this->menu as $uri => $item) { ?>
						<li>
							<a href="<?php
								if($uri{0} == '/') {
									echo Controller::createUrl($uri);
								} else
									echo $uri;
                                ?>">
								<?php echo $item; ?>
							</a>
						</li>
					<?php } ?>
					<li>
						<span id="language" style="font-style: italic;">
							<?php echo Yii::app()->languages->getLanguageTitle(Yii::app()->languages->currentLanguage); ?>
						</span>

						<?php echo CHtml::dropDownList('language', Yii::app()->languages->currentLanguage, Yii::app()->languages->languages, Array(
							'id' => 'language_selector',
							'style' => 'display: none;'
						)); ?>
					</li>
				</ul>
				<div class="pull-right">
                                    <a href="http://ev.kde.org">
                                        <img class="logo" src="<?php echo Yii::app()->baseUrl;?>/css/images/logo_ev.png" alt="KDE e.V." />
                                    </a>
				
                                    <a href="http://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
				</div>
			</div>
		</div>
	</body>
</html>
