<?php $form=$this->beginWidget('CActiveForm', array(
	'htmlOptions' => array(
		'autocomplete' => 'off'
	)
));

?>

<div class="column">
	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<br />
		<?php echo $form->textField($model,'first_name'); ?>
		<?php echo $form->error($model, 'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<br />
		<?php echo $form->textField($model,'last_name'); ?>
		<?php echo $form->error($model, 'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<br />
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'birth_date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'model' => $model,
			'name' => 'Member[birth_date]',
			'value' => $model->birth_date ? Yii::app()->format->formatDate($model->birth_date) : '',
			'language' => Yii::app()->languages->currentLanguage !== 'en' ? Yii::app()->languages->currentLanguage : '',
		)); ?>
		<?php echo $form->error($model, 'birth_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'public'); ?>
		<br />
		<?php echo $form->checkBox($model,'public'); ?>
		<?php echo $form->error($model, 'public'); ?>
	</div>

	<?php
	if($isAdmin) {
	?>
		<div class="row">
			<?php echo $form->labelEx($model, 'isAdmin'); ?>
			<br />
			<?php echo $form->checkBox($model, 'isAdmin'); ?>
			<?php echo $form->error($model, 'isAdmin'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model, 'is_passive'); ?>
			<br />
			<?php echo $form->checkBox($model, 'is_passive'); ?>
			<?php echo $form->error($model, 'is_passive'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model, 'number'); ?>
			<br />
			<?php echo $form->textField($model, 'number'); ?>
			<?php echo $form->error($model, 'number'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model, 'package_sent'); ?>
			<br />
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
				'model' => $model,
				'name' => 'Member[package_sent]',
				'value' => $model->package_sent ? Yii::app()->format->formatDate($model->package_sent) : ''
			)); ?>
			<?php echo $form->error($model, 'package_sent'); ?>
		</div>

		<div class="row">
			<?php
			if($model->update_time) {
				echo $form->labelEx($model, 'update_time');
				echo '<br />';
				echo Yii::app()->format->formatDate($model->update_time);
			}

			echo '<br />';

			if($model->updater) {
				echo $form->labelEx($model, 'updater');
				echo '<br />';
				echo Member::model()->findByPK($model->updater)->fullName;
			}
			?>
		</div>

		<div class="row">
			<?php
			if($model->register_time) {
				echo $form->labelEx($model, 'register_time');
				echo '<br />';
				echo Yii::app()->format->formatDate($model->register_time);
			}
			?>
		</div>
	<?php
	}
	?>
</div>

<div class="column">
	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<br />
		<?php echo $form->textArea($model,'address'); ?>
		<?php echo $form->error($model, 'address'); ?>
	</div>

	<div class="row">
		<?php $model->password = Null; ?>
		<?php echo $form->labelEx($model,'password'); ?>
		<br />
		<?php echo $form->passwordField($model, 'password'); ?>
		<?php echo $form->error($model, 'password'); ?>
	</div>

	<div class="row">
		<?php $model->passwordConfirmation = Null; ?>
		<?php echo $form->labelEx($model,'passwordConfirmation'); ?>
		<br />
		<?php echo $form->passwordField($model, 'passwordConfirmation'); ?>
		<?php echo $form->error($model, 'passwordConfirmation'); ?>
	</div>

	<?php if($isAdmin) {?>
		<div class="row">
			<?php echo $form->labelEx($model,'comments'); ?>
			<br />
			<?php echo $form->textArea($model,'comments'); ?>
			<?php echo $form->error($model, 'comments'); ?>
		</div>
	<?php } ?>

	<div class="row">
		<?php
		$value = isset($buttonValue) ? $buttonValue : Yii::t('member', 'Save');
		echo CHtml::submitButton($value);
		?>
	</div>
</div>


<script type="text/javascript">
	$('form').submit(function() {
		$('<input type="hidden" name="captcha" />').val($('#Member_email').val()).appendTo(this);
	});
</script>
<?php $this->endWidget(); ?>
