<?php
$dataProvider = New CActiveDataProvider('Member', array(
		'criteria' => array(
			'condition' => 'public = 1 AND number>0 AND is_passive = 0',
			'order' => 'first_name'
		),
		'pagination' => False
	));
?>
<ul id="members">
	<?php foreach($dataProvider->data as $member) { ?>
		<li>
			<div class="name">
				<?php echo $member->fullName; ?>
			</div>
			<div class="since">
				<?php echo Yii::t('member', 'Since :date', Array(
					':date' => date('M Y', $member->firstPayment->date)
				));?>
			</div>
		</li>
	<?php } ?>
</ul>