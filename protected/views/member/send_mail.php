<?php $form = $this->beginWidget('CActiveForm'); ?>
	<div class="help">
		Sending emails might take a while. Do not refresh the page. You will cause duplicate emails!
		<br />
		You can use the following variables in body:
		<table>
			<tr>
				<td>
					Variable
				</td>
				<td>
					Description
				</td>
				<td>
					Example
				</td>
			</tr>
			<?php
				$example = current($model->recievers);

				foreach($variables as $attribute) {
			?>
				<tr>
					<td>
						:<?php echo $attribute;?>
					</td>
					<td>
						<?php echo $model->getAttributeLabel($attribute); ?>
					</td>
					<td>
						<?php echo $example->{$attribute}; ?>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>
	<br />

	<div class="row">
		<?php echo $form->labelEx($model, 'from'); ?>
		<br />
		<?php echo $form->textField($model, 'from'); ?>
		<br />
		<?php echo $form->error($model, 'from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'subject'); ?>
		<br />
		<?php echo $form->textField($model, 'subject'); ?>
		<br />
		<?php echo $form->error($model, 'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'body'); ?>
		<br />
		<?php echo $form->textArea($model, 'body'); ?>
		<br />
		<?php echo $form->error($model, 'body'); ?>
	</div>

	<div class="row">
		<?php
		echo CHtml::checkBoxList('recievers[]', $recievers, CHtml::listData($model->recievers, 'id', 'fullName'));
		?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(); ?>
	</div>
<?php $this->endWidget(); ?>