<a href="<?php echo Controller::createUrl('/member/create');?>">
	Add new member
</a>

<?php
Yii::import('zii.widgets.grid.CGridColumn');

Class NextPaymentColumn Extends CGridColumn {
	public function renderDataCellContent($row, $data) {
		if($data->number < 1)
			return ;
		$days = (Int)(($data->nextPaymentDate - time()) / 86400);
		if($days === 0)
			echo Yii::t('member', 'Passed');
		else
			echo Yii::t('member', ':days Days', array(':days' => $days));
	}
}

Class IsPassiveColumn Extends CGridColumn {
	public function renderDataCellContent($row, $data) {
		if($data->is_passive)
			echo Yii::t('member', 'Passive');
	}
}

echo CHtml::beginForm(Controller::createUrl('/member/send_mail'));

$this->widget('zii.widgets.grid.CGridView', array(
	'cssFile' => False,
	'dataProvider' => $dataProvider,
	'filter' => $model,
	'selectableRows' => 2,
	'columns' => Array(
		'id',
		'number',
		'first_name',
		'last_name',
		'email',
		array(
			'class' => 'IsPassiveColumn',
			'header' => Yii::t('member', 'Passive?')
		),
		array(
			'class' => 'NextPaymentColumn',
			'header' => Yii::t('member', 'Next payment')
		),
		array(
			'class' => 'CCheckBoxColumn',
			'header' => Yii::t('member', 'Send email'),
			'name' => 'id',
			'checkBoxHtmlOptions' => array(
				'name' => 'recievers[]'
			),
			'headerHtmlOptions' => Array(
				'style' => 'width:20px'
			)
		),
		array(
			'class' => 'CButtonColumn',
			'template' => '{update}{delete}{payments}',

			'buttons' => Array(
				'payments' => array(
					'label' => Yii::t('member', 'List of payments'),
					'url' => 'Yii::app()->createUrl("/payment/admin", array("member" => $this->grid->dataProvider->data[$row]->id))',
					'imageUrl' => '../../css/images/donate.png'
				)
			)
		),
	)
));

echo CHtml::submitButton(Yii::t('member', 'Send an email to selected members'));

echo CHtml::endForm();
?>