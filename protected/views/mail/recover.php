Dear <?php echo $model->fullName;?>,
We have recieved a request for changing the password of your account.
If this request hasnt been submitted by you, you can ignore this email.

If you have lost your password and you want to change it, please click on the following link:
<?php echo Controller::createAbsoluteUrl('/recover/finish', array('email' => $model->email, 'recovery' => $model->recovery)); ?>