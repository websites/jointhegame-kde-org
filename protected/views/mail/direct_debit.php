Dear <?php echo $model->fullName; ?>,

Welcome to the supporting membership! We are very happy you have chosen to join us in becoming part of KDE. Your support helps us to further improve our efforts in enabling the KDE community to make great software.

Thanks to your financial support, we will be able to send more contributors to meetings, improve our promotional efforts, keep our infrastructure running and much more. Moreover, the fact that you support us this way means a lot to us in itself - it is great to see that people care about our work.

We hope to engage you in the future, asking you for input on our plans and allowing you to talk to other supporting members and to our developer community. For now, you will receive a little surprise within the next few weeks. Therefore, please make sure that you have added your complete postal address to your account.

The payment method you have chosen is the direct debit authorization. We will debit the amounts at the respective dates according to your specifications. If you have not done so already, please download and fill out the authorisation form (available from http://jointhegame.kde.org/form.pdf) and return this to our office:

 

K Desktop Environment e.V.

Linienstraße 141

10115 Berlin

Germany


Please feel free to contact Claudia Rauch (rauch@kde.org) at any point with suggestions, questions or complaints.

Once again, welcome to the KDE community,


Cornelius Schumacher, president of KDE e.V.,
The whole e.V. membership,
and everybody else in KDE!