Dear <?php echo $model->fullName; ?>,

Welcome to the supporting membership! We are very happy you have chosen to join us in becoming part of KDE. Your support helps us to further improve our efforts in enabling the KDE community to make great software.

Thanks to your financial support, we will be able to send more contributors to meetings, improve our promotional efforts, keep our infrastructure running and much more. Moreover, the fact that you support us this way means a lot to us in itself - it is great to see that people care about our work.

We hope to engage you in the future, asking you for input on our plans and allowing you to talk to other supporting members and to our developer community. For now, you will receive a little surprise within the next few weeks. Therefore, please make sure that you have added your complete postal address to your account.

You have chosen to directly transfer your membership dues to KDE e.V.'s account:

 

Account Name: K Desktop Environment e.V.

Name of Bank: Deutsche Bank Privat- und Geschaeftskunden

Account Number: 0666446

Bank Sort code: 20070024

IBAN code: DE82200700240066644600

BIC or Swift code: DEUTDEDBHAM

Bank’s Location: Hamburg, Germany

 

We regret that we do not yet have online payment facilities or an automatic payment receipts system as yet, and so we strongly advise that when paying into the KDE e.V. account, you include your name and the payment period (e.g. Max Sample , Join the Game Q4 2010).


Please feel free to contact Claudia Rauch (rauch@kde.org) at any point with suggestions, questions or complaints.

Once again, welcome to the KDE community,


Cornelius Schumacher, president of KDE e.V.,
The whole e.V. membership,
and everybody else in KDE!