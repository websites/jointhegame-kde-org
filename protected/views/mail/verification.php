Thank you for registering on KDE's Join The Game campaign.
In order to complete your membership, use the following verification code: <?php echo $model->verification;?>

You can also click on the following link:
<?php echo Controller::createAbsoluteUrl('/join/verify/', array('email' => $model->email, 'verification' => $model->verification)); ?>