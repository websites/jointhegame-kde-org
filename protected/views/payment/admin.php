<a href="<?php echo Controller::createUrl('/payment/create');?>">
	<?php echo Yii::t('payment', 'Add new payment');?>
</a>
<br />
<a href="<?php echo Controller::createUrl('/paypal/recurringprofiles');?>">
	<?php echo Yii::t('payment', 'View all automatic payments');?>
</a>
<br />
<a href="<?php echo Controller::createUrl('/payment/estimate');?>">
	<?php echo Yii::t('payment', 'Estimate payments');?>
</a>
<hr />
<br />

<?php
$this->renderPartial('_payments', array('dataProvider' => $dataProvider));
?>