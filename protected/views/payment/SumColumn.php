<?php
Yii::import('zii.widgets.grid.CDataColumn');

Class SumColumn Extends CDataColumn {
	public $name = 'amount';
	public $doneSum = 0;
	public $allSum = 0;

	public function getHasFooter() {
		return True;
	}

	public function renderDataCellContent($row, $data) {
		$amount = $data->{$this->name};
		echo $amount;
		$this->allSum += (float)$amount;
		if($data->status == Payment::Done)
			$this->doneSum += $amount;
	}

	public function renderFooterCellContent() {
		echo Yii::t('payment', 'Total: :done/:all', array(':done' => $this->doneSum, ':all' => $this->allSum));
	}
}