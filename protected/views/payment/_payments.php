<?php
$dataProvider->pagination = False;

if(!$columns) {
	$columns = Array(
		'id',
		'owner.fullName',
		'date:date',
		'methodTitle',
		'status',
		array(
			'class' => 'SumColumn'
		),
	);
}

if(Member::model()->findByPk(Yii::app()->user->id)->isAdmin) {
	$columns[] = array(
		'class' => 'CButtonColumn',
		'template' => '{update}{delete}'
	);
}

Yii::import('application.views.payment.SumColumn');
$this->widget('zii.widgets.grid.CGridView', array(
	'cssFile' => False,
	'dataProvider' => $dataProvider,
	'rowCssClassExpression' => '$data->status',
	'columns' => $columns
));