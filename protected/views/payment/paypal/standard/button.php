<?php $config=new CConfiguration(Yii::getPathOfAlias('application.config').'/paypal.php'); ?>
<form id="paypal" action="<?php echo $config['url']; ?>" method="post">
	<input type="hidden" name="business" value="<?php echo $config['business']; ?>" />
	<input type="hidden" name="cmd" value="_donations" />
	<input type="hidden" name="notify_url" value="<?php echo Yii::app()->createAbsoluteUrl('/paypal/standard/instant'); ?>" />
	<input type="hidden" name="custom" value="<?php echo Yii::app()->user->id; ?>" />
	<input type="hidden" name="return" value="<?php echo Yii::app()->createAbsoluteUrl('/paypal/standard/resume'); ?>" />
	<input type="hidden" name="amount" value="<?php echo $model->amount; ?>" />
	<input type="hidden" name="currency_code" value="EUR" />
	<input type="hidden" name="lc" value="US" />
	<input type="image" name="submit" border="0" src="https://www.paypal.com/en_US/i/btn/btn_donate_LG.gif" alt="PayPal - The safer, easier way to pay online" />
	<img alt="" border="0" width="1" height="1" src="https://www.paypal.com/en_US/i/scr/pixel.gif" />
</form>

<script type="text/javascript">
	$(window).ready(function() {
		$('#paypal').submit();
	});
</script>