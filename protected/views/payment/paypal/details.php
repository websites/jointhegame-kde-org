<?php
$form = $this->beginWidget('CActiveForm', array(
	'action' => Controller::createUrl('/paypal/start')
));
?>
	<div class="row">
		Enable automatic recurring payments?
		<br />
		<?php echo $form->checkBox($model, 'isRecurring'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(); ?>
	</div>
<?php $this->endWidget(); ?>