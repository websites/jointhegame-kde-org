<table>
	<tr>
		<td>
			<?php echo Yii::t('payment', 'Profile ID'); ?>
		</td>
		<td>
			<?php echo $model->details[PaypalPayment::Profile]; ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Status'); ?>
		</td>
		<td>
			<?php echo $model->details['STATUS']; ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Description'); ?>
		</td>
		<td>
			<?php echo $model->details['DESC']; ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Amount'); ?>
		</td>
		<td>
			<?php echo $model->details[PaypalPayment::Amount].' '.$model->details[PaypalPayment::Currency] ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Start date'); ?>
		</td>
		<td>
			<?php echo Yii::app()->format->formatDate(strtotime($model->details[PaypalPayment::StartDate])); ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Last payment'); ?>
		</td>
		<td>
			<?php echo Yii::app()->format->formatDate(strtotime($model->details['LASTPAYMENTDATE'])); ?>
		</td>
	</tr>

	<tr>
		<td>
			<?php echo Yii::t('payment', 'Next scheduled payment'); ?>
		</td>
		<td>
			<?php echo Yii::app()->format->formatDate(strtotime($model->details['NEXTBILLINGDATE'])); ?>
		</td>
	</tr>
</table>

<br />

<?php
$dataProvider = New CActiveDataProvider('Payment', array());
$dataProvider->criteria->addInCondition('transaction', $model->transactions);

$this->renderPartial('/payment/_payments', array(
	'dataProvider' => $dataProvider,
	'columns' => Array(
		'date:date',
		'status',
		array(
			'class' => 'SumColumn'
		)
	)
	)
);
?>