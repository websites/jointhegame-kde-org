<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'cssFile' => False,
	'dataProvider' => New CActiveDataProvider('PaypalRecurringProfile'),
	'columns' => array(
		'id',
		'owner.fullName',
		array(
			'class' => 'CButtonColumn',
			'viewButtonUrl' => 'Yii::app()->createUrl("/paypal/recurringProfile", array("id" => $data->id))',
			'template' => '{view}'
		)
	)
));