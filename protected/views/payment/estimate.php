<?php
$this->beginWidget('CActiveForm', array('method' => 'get'));

	echo CHtml::label(Yii::t('payment', 'From'), 'from').'&nbsp';
	echo '<br />';
	$this->widget('zii.widgets.jui.CJuiDatePicker', Array(
		'id' => 'from',
		'name' => 'from',
		'value' => $from
	));

	echo '<br />';

	echo CHtml::label(Yii::t('payment', 'To'), 'to');
	echo '<br />';
	$this->widget('zii.widgets.jui.CJuiDatePicker', Array(
		'id' => 'to',
		'name' => 'to',
		'value' => $to
	));

	echo '<br /><br />';

	echo CHtml::submitButton();
$this->endWidget();

if($members) {
	$total = 0;
	$expectedToPay = Array();

	foreach($members as $member) {
		if(($member->nextPaymentDate > strtotime($from)) && ($member->nextPaymentDate < strtotime($to))) {
			$total += $member->lastPaymentAmount;
			$expectedToPay[] = $member;
		}
	}

	echo '<br /><h1>'.Yii::t('payment', 'We estimate :amount euros to be payed from :from to :to. Below, is a detailed list of expected payments.', Array(
		':amount' => Yii::app()->format->formatNumber($total),
		':from' => Yii::app()->format->formatDate(strtotime($from)),
		':to' => Yii::app()->format->formatDate(strtotime($to))
	)).'</h1><br />';

	if(!$columns) {
		$columns = Array(
			'number:number:'.Yii::t('payment', 'Number'),
			'fullName:raw:'.Yii::t('payment', 'Full Name'),
			'nextPaymentDate:date:'.Yii::t('payment', 'Expected payment date'),
			'lastPaymentAmount:number:'.Yii::t('payment', 'Expected payment amount')
		);
	}

	$this->widget('zii.widgets.grid.CGridView', array(
		'cssFile' => False,
		'dataProvider' => New CArrayDataProvider($expectedToPay, Array(
			'pagination' => False,
			'sort' => Array(
				'attributes' => array('fullName', 'nextPaymentDate', 'lastPaymentAmount'),
				'defaultOrder' => 'nextPaymentDate'
			)

		)),
		'columns' => $columns
	));
}