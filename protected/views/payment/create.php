<?php $form = $this->beginWidget('CActiveForm'); ?>

	<?php
		$this->extra = CHtml::errorSummary($model);
	?>

	<div class="row">
		<?php echo $form->labelEx($model, 'member'); ?>
		<br />
		<?php
// 		var_dump($model->member, $model->owner);
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'name'=>'member_label',
			'value' => $model->owner ? $model->owner->fullName.' ('.$model->owner->email.')' : '',
			'sourceUrl'=>Yii::app()->createUrl('/member/search'),
			'options' => Array(
				'select' => 'js:function(event, ui) {
					$("#payment_member").val(ui.item.id);
				}'
			)
		));
		?>
		<?php echo $form->hiddenField($model, 'member', array('id' => 'payment_member')); ?>
		<br />
		<?php echo $form->error($model, 'member'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'amount'); ?>
		<br />
		<?php echo $form->textField($model, 'amount'); ?>
		<br />
		<?php echo $form->error($model, 'amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'model' => $model,
			'attribute' => 'formattedDate'
		)); ?>
		<br />
		<?php echo $form->error($model, 'formattedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'method'); ?>
		<br />
		<?php echo $form->dropDownList($model, 'method', CHtml::listData($model->methods, 'method', 'title')); ?>
		<br />
		<?php echo $form->error($model, 'method'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'status'); ?>
		<br />
		<?php echo $form->dropDownList($model, 'status', $model->statuses); ?>
		<br />
		<?php echo $form->error($model, 'status'); ?>
	</div>

	<div class="row">
		<?php
			echo CHtml::submitButton();
			if(!$model->isNewRecord && $model->couldAutomaticallyUpdate)
				echo CHtml::submitButton(Yii::t('payment', 'Sync'), array('name' => 'automaticUpdate'));
		?>
	</div>
<?php $this->endWidget(); ?>