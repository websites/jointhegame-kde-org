<?php $form = $this->beginWidget('CActiveForm'); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<br />
		<?php echo $form->textField($model, 'email'); ?>
		<br />
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<?php echo CHtml::submitButton(Yii::t('join', 'Next')); ?>
<?php $this->endWidget(); ?>