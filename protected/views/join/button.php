<?php if(Yii::app()->user->isGuest) {?>
	<div id="join_button">
                <div class="action">
                    <a class="action-button" href="<?php echo Controller::createUrl('/join/start') ?>">
			<?php echo Yii::t('join', 'Join now!'); ?>
                    </a>
                </div>
	</div>
<?php } else {
	$member = Member::model()->findByPk(Yii::app()->user->id);
	echo Yii::t('join', 'Welcome :fullName.', array(':fullName' => $member->fullName));
	echo(' ');
	$days = ($member->nextPaymentDate - time())/86400;

	if($days > 1)
		echo Yii::t('join', 'Your next payment is due in :days days.', array(':days' => $days));
	else {
		if($member->lastPayment)
			echo Yii::t('join', 'Your membership is expired.');
		else
			echo Yii::t('join', 'Your membership is not completed yet.');

		?>

		<p>
		We are now using <a href="http://relate.kde.org/">http://relate.kde.org/</a> to handle Join the Game. Please go there
		to renew your membership. Do not hesitete to mail us at <a href="mailto:jtg@kde.org">jtg@kde.org</a> if you have any doubt.
		</p>

		<!--<div id="join_button">
                        <div class="action">
                            <a class="action-button" href="<?php /*echo Controller::createUrl('/join/payment')*/ ?>">
                                <?php /*echo Yii::t('pay', 'Pay now!');*/ ?>
                            </a>
                        </div>
		</div>-->
		<?php
	}
}
?>
