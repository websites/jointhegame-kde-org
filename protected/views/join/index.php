<div id="reasons">
	<ul>
		<li>
			<h1>
				<?php echo Yii::t('join', 'Why join?'); ?>
			</h1>

			<?php echo Yii::t('join', 'Help create beautiful software that gives you full freedom and protects your privacy.'); ?>
			<br /><br />
			<?php echo Yii::t('join', 'Be part of a great community.'); ?>
			<br /><br />
			<?php echo Yii::t('join', 'Become&nbsp;an&nbsp;official&nbsp;supporting member of KDE eV.'); ?>
		</li>

		<li>
			<h1>
				<?php echo Yii::t('join', 'You can help'); ?>
			</h1>
			<ul>
				<?php echo Yii::t('join', 'Your&nbsp;money&nbsp;funds&nbsp;contributor meetings and infrastructure.'); ?>
				<br /><br />
				<?php echo Yii::t('join', 'It keeps our software safe for the future and raises awareness for Free Software and Free Culture.'); ?>
				<br /><br />
				<?php echo Yii::t('join', 'Support our annual conferences Akademy, Camp KDE and KDE.in.'); ?>
			</ul>
		</li>

		<li>
			<h1>
				<?php echo Yii::t('join', 'Your benefits'); ?>
			</h1>
			<?php echo Yii::t('join', 'Invitation to attend the annual general assembly of KDE e.V.'); ?><br /><br />
			<?php echo Yii::t('join', 'Regular first hand reports about KDE\'s activities'); ?><br /><br />
		</li>


		<li>
			<h1>
				<?php echo Yii::t('join', 'About KDE eV'); ?>
			</h1>
			<?php echo Yii::t('join', 'KDE eV is a non-profit organization that represents the KDE Project in legal and financial matters.'); ?>
			<br /><br />
			<?php echo Yii::t('join', 'Read more at the <a href="http://ev.kde.org">eV site</a> and in the <a href="http://ev.kde.org/reports/">eV reports</a>.'); ?>
		</li>
	</ul>
</div>
<div id="quotes">
	<ul>
		<li class="quote">
			<?php echo Yii::t('join', 'KDE has awesome technology developed by an incredible community, it\'s an honor to join the game
<br />
- Georg Greve, founder FSFE'); ?>
		</li>
	</ul>
</div>
