<?php $form = $this->beginWidget('CActiveForm'); ?>
	<div class="row">
		<?php echo CHtml::label($model->getAttributeLabel('verification'), 'verification'); ?>
		<br />
		<?php echo CHtml::textField('verification', $verification, array('id' => 'verification')); ?>
		<br />
		<?php echo $form->error($model, 'verification'); ?>
	</div>

	<?php echo CHtml::submitButton(Yii::t('join', 'Next')); ?>
<?php $this->endWidget(); ?>