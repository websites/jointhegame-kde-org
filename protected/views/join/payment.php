<?php $this->beginWidget('CActiveForm'); ?>
	<div class="row">
		<?php echo CHtml::label(Yii::t('join', 'Payment method'), 'method'); ?>
		<br />
		<?php echo CHtml::dropDownList('method', $method, CHtml::listData(Payment::getMethods(), 'method', 'title'), array('id' => 'method')); ?>
	</div>

	<div class="row">
		<?php echo CHtml::label(Yii::t('join', 'Payment period'), 'period'); ?>
		<br />
		<?php echo CHtml::radioButtonList('period', $period, Payment::getPeriods(), array('id' => 'period')); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(Yii::t('join', 'Next')); ?>
	</div>
<?php $this->endWidget(); ?>