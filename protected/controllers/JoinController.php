<?php
Class JoinController Extends Controller {
	public function accessRules() {
		return Array(
			Array(
				'deny',
				'actions' => array('start', 'email', 'information'),
				'users' => array('@')
			),

			Array(
				'deny',
				'actions' => array('payment', 'finish'),
				'users' => array('?')
			)
		);
	}

	public function actionIndex() {
		$this->title = Yii::t('join', 'Become a supporting member of KDE for only 100 €/year');
		$this->extra = $this->renderPartial('/join/button', array(), True);
		$this->render('index');
	}

	public function actionStart() {
		$this->redirect(Controller::createUrl('/join/information'));
	}

	public function actionVerification() {
		$this->showJoinStuff = False;

		$model = Member::model()->findByPK(Yii::app()->user->id);

		$this->title = Yii::t('join', 'Enter the verification code we sent to :email', array(':email' => $model->email));

		if($_POST['verification']) {
			if($_POST['verification'] == $model->verification) {
				$model->verification = '';
				$model->save();
				$this->redirect(Controller::createUrl('/join/payment'));
			} else
				$model->addError('verification', Yii::t('join', 'The verification code you entered is wrong'));

		}
		$this->render('verification', array(
			'model' => $model,
			'verification' => $_POST['verification']
		));
	}

	public function actionVerify() {
		$model = Member::model()->findByAttributes(array('email' => $_GET['email']));

		if($model && $model->verification == $_GET['verification']) {
			$model->verification = '';
			$model->save();
			$this->redirect(Controller::createUrl('/join/payment'));
		}

		$this->redirect(Controller::createUrl('/'));
	}

	public function actionInformation() {
		$this->redirect('https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5');
		return ;

		$this->showJoinStuff = False;

		$model = New Member;
		$model->scenario = 'info';

		$this->title = Yii::t('join', 'Enter your personal information');

		if($_POST['Member']) {
			$model->attributes = $_POST['Member'];

			if($_POST['Member']['password'])
				$model->password = md5($_POST['Member']['password']);

			if($_POST['Member']['passwordConfirmation'])
				$model->passwordConfirmation = md5($_POST['Member']['passwordConfirmation']);

			$model->verification = rand(1000, 9999);
			$model->birth_date = strtotime($_POST['Member']['birth_date']);

			if($_POST['Member']['email'] !== $_POST['captcha'])
				$model->addError('email', Yii::t('join', 'You either have javascript disabled or you are a spambot. In case of the former, please enable javascript on your browser.'));

			if(!count($model->errors) && $model->save()) {
				Yii::import('ext.yii-mail.*');
				$mail = New YiiMailMessage;
				$mail->subject = Yii::t('join', "Your verification number on KDE's Join The Game");
				$mail->from = 'jointhegame-admin@kde.org';
				$mail->to = array($model->email);
				$mail->body = $this->renderPartial('/mail/verification', array('model' => $model), True, True);
				Yii::app()->mail->send($mail);

				$recievers = New CConfiguration('protected/config/notifications.php');
				$mail = New YiiMailMessage;
				$mail->subject = Yii::t('join', "New member on KDE's Join The Game");
				$mail->from = 'jointhegame-admin@kde.org';
				$mail->to = $recievers['recievers'];
				$mail->body = $this->renderPartial('/mail/notification', array('model' => $model), True, True);
				Yii::app()->mail->send($mail);

				$identity = New MemberIdentity($model->email, $_POST['Member']['password']);

				if($identity->authenticate()) {
					Yii::app()->user->login($identity);
					$this->redirect(Controller::createUrl('/join/payment'));
				}
			}
		}

		$this->render('information', array('model' => $model));
	}

	public function actionPayment() {
		$this->showJoinStuff = False;

		if(Yii::app()->user->isGuest)
			$this->redirect(Controller::createUrl('/join/start'));

		$amount = 100;

		$this->title = Yii::t('join', 'Enter payment information');

		$model = Member::model()->findByPK(Yii::app()->user->id);

		$method = $_POST['method'];
		$period = $_POST['period'] ? $_POST['period'] : Payment::ANNUALY;

		if($method) {
			$payment = Payment::model($method);
			$payment->isNewRecord = True;

			if($period == Payment::QUARTERLY) {
				$amount = $amount / 4;
				$payment->period =  91;
			} else {
				$payment->period = 365;
			}

			$payment->amount = $amount;
			$payment->member = $model->id;
			$payment->date   = time();
			$payment->pay(Controller::createUrl('/join/finish'));
			return ;
		}

		$this->render('payment', array(
			'model' => $model,
			'method' => $method,
			'period' => $period
		));
	}

	public function actionFinish() {
		$this->showJoinStuff = False;

		if(Yii::app()->user->isGuest)
			$this->redirect(Controller::createUrl('/join/start'));


		$model = Member::model()->findByPK(Yii::app()->user->id);

		$this->title = Yii::t('join', 'Thank You.');

		$this->render('finish', array('model' => $model));
	}
}
?>