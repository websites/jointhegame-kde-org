<?php
Class PaymentController Extends Controller {
	public function accessRules() {
		return Array(
			array(
				'allow',
				'actions' => array('admin', 'create', 'update', 'delete', 'estimate'),
				'roles' => array('admin')
			),

			array(
				'deny'
			)
		);
	}

	public function actionAdmin() {
		$dataProvider = New CActiveDataProvider('Payment');
		if($_GET['member'])
			$dataProvider->criteria->condition = 'member = '.$_GET['member'];

		$this->render('admin', array('dataProvider' => $dataProvider));
	}

	public function actionCreate() {
		$model = New Payment;
		$model->scenario = 'create';

		if($_POST['Payment']) {
			$model->attributes = $_POST['Payment'];
			$model->date = CDateTimeParser::parse($_POST['Payment']['formattedDate']);

			if($model->validate()) {
				$name = $_POST['Payment']['method'].'Payment';
				$model = New $name;
				$model->scenario = 'create';
				$model->attributes = $_POST['Payment'];
				$model->date = strtotime($_POST['Payment']['formattedDate']);

				if($model->save())
					$this->redirect(Controller::createUrl('/payment/admin'));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate() {
		$model = Payment::model()->findbyPK($_GET['id']);
		$model->scenario = 'create';

		if($_POST[get_class($model)]) {
			if($_POST['automaticUpdate'])
				$model->automaticUpdate();
			else {
				$model->attributes = $_POST[get_class($model)];
				$model->date = strtotime($_POST[get_class($model)]['formattedDate']);
				$model->save();
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionDelete() {
		$payment = Payment::model()->findByPK($_GET['id']);
		$payment->delete();
	}

	public function actionEstimate() {
		if($_GET['from'] && $_GET['to']) {
			$members = Member::model()->findAll(Array(
				'condition' => 'number > 0'
			));
		} else
			$members = Array();

		$this->render('estimate', Array('members' => $members, 'from' => $_GET['from'], 'to' => $_GET['to']));
	}
}