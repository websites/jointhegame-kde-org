<?php
Class PaypalController Extends Controller {
	public function accessRules() {
		return Array(
			array(
				'allow',
				'actions' => array('start', 'resume'),
				'users' => array('@')
			),

			array(
				'allow',
				'actions' => array('recurringprofile', 'recurringprofiles'),
				'roles' => array('admin')
			),

			array(
				'deny'
			)
		);
	}

	public function actionStart() {
		$payment = Yii::app()->session->get('paypal');

		if($_POST['PaypalPayment']['isRecurring']) {
			$payment->isRecurring = True;
		}

		$payment->start();
	}

	public function actionResume() {
		$paypal = Yii::app()->session->get('paypal');
		$paypal->token = $_GET['token'];

		$result = $paypal->saveAutomatically();

		if($result)
			$this->redirect($paypal->redirectUrl);
	}

	public function actionRecurringProfiles() {
		$this->render('/payment/paypal/recurring_profiles');
	}

	public function actionRecurringProfile() {
		$model = PaypalRecurringProfile::model()->findByPK($_GET['id']);
		$model->update();

		$this->title = Yii::t('payment', 'Recurring payment for :fullName', array(
					':fullName' => $model->owner->fullName
				));

		$this->render('/payment/paypal/recurring_profile', array('model' => $model));
	}
}