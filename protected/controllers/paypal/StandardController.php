<?php
Class StandardController Extends Controller {
	public function actionResume() {
		$this->title = Yii::t('join', 'Thank you for supporting KDE!');
		$this->render('/member/widgets');
	}

	public function actionInstant() {
		New Payment;
		$payment = New StandardPaypalPayment;
		$payment->saveAutomatically($_POST);
	}
}