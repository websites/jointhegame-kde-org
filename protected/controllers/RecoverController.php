<?php
Class RecoverController Extends Controller {
	public function accessRules() {
		return Array(
			Array(
				'deny',
				'actions' => array('index', 'finish'),
				'users' => array('@')
			)
			
		);
	}

	public function actionIndex() {
		$this->title = Yii::t('member', 'Recover your password');
		$model = Member::model();
		$model->scenario = 'recover';

		if($_POST['Member']) {
			$model->email = $_POST['Member']['email'];

			if($model->validate()) {
				$claimed = Member::model()->findByAttributes(array('email' => $model->email));
				if($claimed) {
					$model = $claimed;
					$model->recovery = rand(1000, 99999);
					$model->save(False);

					Yii::import('ext.yii-mail.*');
					$mail = New YiiMailMessage;
					$mail->subject = Yii::t('join', "Recover password on KDE's Join The Game");
					$mail->from = 'jointhegame-admin@kde.org';
					$mail->to = array($claimed->email);
					$mail->body = $this->renderPartial('/mail/recover', array('model' => $model), True, True);

					Yii::app()->mail->send($mail);

					$this->addMessage(Yii::t('member', Yii::t('member', 'Please check your inbox and click on the provided link.')));
					$this->renderText('');
					return ;
				} else
					$model->addError('email', Yii::t('member', 'Entered email address is not registered'));

			}
		}

		$this->render('recover', array('model' => $model, 'recover' => isset($recover)));
	}

	public function actionFinish() {
		if(!$_GET['email'] || !$_GET['recovery'])
			$this->redirect(Controller::createUrl('/'));

		$model = Member::model()->findByAttributes(array('email' => $_GET['email'], 'recovery' => $_GET['recovery']));
		if(!$model)
			$this->redirect(Controller::createUrl('/'));

		$password = rand(100000, 999999);
		$model->recovery = '';
		$model->password = md5($password);
		$model->save(False);

		$this->addMessage(Yii::t('member', 'Your new password is: :password', array(':password' => $password)));
		$this->renderText('');
	}
}