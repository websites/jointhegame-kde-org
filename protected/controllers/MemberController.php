<?php
Class MemberController Extends Controller {
	public function accessRules() {
		return array(
			array(
				'deny',
				'actions' => array('login'),
				'users' => array('@')
			),

			array(
				'allow',
				'actions' => array('login'),
				'users' => array('?')
			),

			array(
				'allow',
				'actions' => array('logout', 'payments', 'profile', 'widgets'),
				'users' => array('@')
			),

			array(
				'allow',
				'actions' => array('index', 'widget')
			),

			array(
				'allow',
				'actions' => array('create', 'view', 'admin', 'update', 'delete', 'profile', 'send_mail', 'search', 'payment_passed'),
				'roles' => array('admin')
			),

			array(
				'deny'
			),
		);
	}

	public function actionIndex() {
		$this->redirect('https://relate.kde.org/civicrm/profile?gid=18&search=0');

// 		$this->title = Yii::t('member', 'We have :count supportings members. Here are those who chose to be listed publicly.', Array(
// 			':count' => Member::model()->count('number > 0 AND is_passive = 0')
// 		));
//
// 		$this->render('index');
	}

	public function actionWidgets() {
		$member = Member::model()->findByPK(Yii::app()->user->id);
		if($member->firstPayment) {
			$this->title = Yii::t('member', 'Be proud and help KDE even more!');
			$this->render('widgets');
		} else
			$this->redirect(array('/join/payment'));
	}

	public function actionWidget() {
		$name = $_GET['widget'].'Widget';
		Yii::import('application.widgets.'.$name);

		$member = Member::model()->findbyPK($_GET['member']);
		if($member && $member->firstPayment) {
			$widget = New $name;
			$widget->member = $member;
			Controller::redirect($widget->outputUrl);
		}
	}

	public function actionCreate() {
		$this->title = Yii::t('member', 'Create a member');

		$model = New Member;
		$model->scenario = 'info';

		if($_POST['Member']) {
			$model->attributes = $_POST['Member'];
			$model->number = $_POST['Member']['number'];

			$model->package_sent = strtotime($_POST['Member']['package_sent']);
			$model->birth_date = strtotime($_POST['Member']['birth_date']);
			$model->is_passive = (Boolean)$_POST['Member']['is_passive'];
			$model->comments = $_POST['Member']['comments'];

			if($model->save()) {
				$this->addMessage(Yii::t('member', ":fullName's account with id ':id' has been created successfully.", array(
					':fullName' => $model->fullName,
					':id' => $model->id
				)));

				$_GET['id'] = $model->id;
				$_POST = Array();
				$this->actionUpdate();
				return ;
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionAdmin() {
		$this->title = Yii::t('member', 'Members list');

		$criteria = New CDbCriteria;

		$model = Member::model();
		if(is_array($_GET['Member']))
			foreach($_GET['Member'] as $attribute => $value)
				if($value) {
					$criteria->addSearchCondition($attribute, $value);
					$model->$attribute = $value;
				}

		$provider = New CActiveDataProvider('Member');
		$provider->criteria = $criteria;
		$provider->pagination = False;

		$this->render('admin', array('dataProvider' => $provider, 'model' => $model));
	}

	public function actionUpdate() {
		$model = Member::model()->findByPK($_GET['id']);

		$this->title = Yii::t('member', 'Update :fullName', array(
			':fullName' => $model->fullName
		));

		if($_POST['Member']) {
			$model->attributes = $_POST['Member'];

			if($_POST['Member']['password'])
				$model->password = md5($_POST['Member']['password']);

			if($_POST['Member']['passwordConfirmation'])
				$model->passwordConfirmation = md5($_POST['Member']['passwordConfirmation']);

			$model->package_sent = strtotime($_POST['Member']['package_sent']);
			$model->birth_date = strtotime($_POST['Member']['birth_date']);
			$model->isAdmin = (Boolean)$_POST['Member']['isAdmin'];
			$model->is_passive = (Boolean)$_POST['Member']['is_passive'];
			$model->comments = $_POST['Member']['comments'];

			if($model->save())
				$this->addMessage(Yii::t('member', ":fullName's profile updated successfully", array(':fullName' => $model->fullName)));
		}

		$this->render('update', array('model' => $model));
	}

	public function actionDelete() {
		$model = Member::model()->findByPK($_GET['id']);
		$model->delete();
	}

	public function actionLogin() {
		$this->title = Yii::t('member', 'Login');

		$model = Member::model();
		$model->scenario = 'login';
		$model->attributes = $_POST['Member'];

		if($_POST && $model->validate()) {
			$identity = New MemberIdentity($model->email, $model->password);
			if($identity->authenticate()) {
				Yii::app()->user->login($identity);
				$this->redirect(Controller::createUrl('/'));
			} else
				$model->addError('password', Yii::t('member', 'Provided username and password are wrong'));
		}

		$this->render('login', array('model' => $model));
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Controller::createUrl('/'));
	}

	public function actionProfile() {
		$model = Member::model()->findByPK(Yii::app()->user->id);
		$model->scenario = 'profile';
		$model->passwordConfirmation = $model>password;

		if($_POST['Member']) {
			$model->attributes = $_POST['Member'];
			$model->birth_date = strtotime($_POST['Member']['birth_date']);

			if($_POST['Member']['password'])
				$model->password = md5($_POST['Member']['password']);

			if($_POST['Member']['passwordConfirmation'])
				$model->passwordConfirmation = md5($_POST['Member']['passwordConfirmation']);

			if($model->save())
				$this->addMessage(Yii::t('member', "Your profile has been updated successfully"));
		}

		$this->title = Yii::t('member', 'Edit profile for :fullName', array(':fullName' => $model->fullName));

		$this->render('profile', array('model' => $model));
	}

	public function actionPayments() {
		$model = Member::model()->findByPK(Yii::app()->user->id);

		$this->title = Yii::t('member', 'My donations');

		if($model->nextPaymentDate > time() + 864400)
			$this->extra = Yii::t('member', 'Next payment scheduled for :date <br /> (:days days)', array(
				':date' => Yii::app()->format->formatDate($model->nextPaymentDate),
				':days' => ($model->nextPaymentDate - time()) / 86400
				)
			);

		$dataProvider = New CActiveDataProvider('Payment');

		$dataProvider->criteria->addCondition('member = '.$model->id);

		$this->render('/payment/_payments', array(
			'dataProvider' => $dataProvider,
			'columns' => Array(
				'date:date',
				'methodTitle',
				'status',
				array(
					'class' => 'SumColumn'
				)
			)
		));
	}

	public function actionSend_Mail() {
		if(count($_POST['recievers']) < 1)
			$this->redirect(Controller::createUrl('/member/admin'));

		$recievers = Array();
		foreach($_POST['recievers'] as $reciever)
			$recievers[] = Member::model()->findByPK($reciever);

		$model = New MailForm;
		$model->attributes = $_POST['MailForm'];
		$model->recievers = $recievers;

		$variables = Array('fullName', 'first_name', 'last_name', 'email', 'address');

		if($model->validate()) {
			Yii::import('ext.yii-mail.*');

			foreach($model->recievers as $reciever) {
				$body = $model->body;
				foreach($variables as $attribute)
					$body = str_replace(':'.$attribute, $reciever->{$attribute}, $body);

				$mail = New YiiMailMessage;
				$mail->subject = $model->subject;
				$mail->from = $model->from;
				$mail->to = $reciever->email;
				$mail->body = $body;

				Yii::app()->mail->send($mail);
			}

			$this->addMessage(Yii::t('member', 'Email sent to :count members.', array(':count' => count($_POST['recievers']))));
			$this->renderText('');
		} else {
			if(!$model->from)
				$model->from = Member::model()->findByPK(Yii::app()->user->id)->email;

			$this->render('send_mail', array(
				'model' => $model,
				'recievers' => $_POST['recievers'],
				'variables' => $variables
			));
		}
	}

	public function actionSearch() {
		$result = Array();

		$term = '"%'.$_GET['term'].'%"';
		$members = Member::model()->findAllBySql("SELECT * FROM member WHERE first_name LIKE $term OR last_name LIKE $term OR email LIKE $term");
		foreach($members as $member)
			$result[] = Array('id' => $member->id, 'label' => $member->fullName.' ('.$member->email.')');

		print json_encode($result);
	}

	public function actionPayment_passed() {
		$members = Member::model()->findAllByAttributes(Array(
			'is_passive' => 0,
			'number' => '> 0'
		));

		$passed = Array();
		$now = time();
		foreach($members as $member) {
			if($member->nextPaymentDate > $now)
				continue;

			$passed[] = $member;
		}

		$this->title = Yii::t('join', ':count (active) members have skipped their payments.', array(':count' => count($passed)));
		$this->renderText('');
	}
}

Class MailForm Extends CFormModel {
	public $subject;
	public $body;
	public $from;
	public $recievers;

	public function rules() {
		return array(
			array('subject, body, recievers, from', 'required'),
			array('subject, body, recievers, from', 'safe'),
			array('from', 'email')
		);
	}

	public function attributeLabels() {
		return Array(
			'subject' => Yii::t('member', 'Subject'),
			'body' => Yii::t('member', 'Body'),
			'from' => Yii::t('member', 'From (email address)')
		);
	}
}