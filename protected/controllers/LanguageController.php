<?php
Class LanguageController Extends Controller {
	public function actionChange() {
		$language = current(array_keys($_GET));
		if(!$language)
			return ;

		if(!Yii::app()->languages->languages[$language])
			return ;

		$cookie = New CHttpCookie('language', $language);
		Yii::app()->request->cookies['language'] = $cookie;

		if($_SERVER['HTTP_REFERER'])
			$this->redirect($_SERVER['HTTP_REFERER']);
		else
			$this->redirect(Array('/'));
	}
}