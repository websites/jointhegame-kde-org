<?php
Class ApiController Extends Controller {
    public function actionMembers() {
        $config = new CConfiguration(Yii::getPathOfAlias('application.config').'/api.php');

        if(!in_array($_SERVER['REMOTE_ADDR'], $config['allowed_ips']))
            die();

        $dataProvider = New CActiveDataProvider('Member', array(
            'criteria' => array(
                'condition' => 'public = 1 AND number>0 AND is_passive = 0',
                'order' => 'first_name'
            ),
            'pagination' => False
        ));

        $res = Array();
        foreach($dataProvider->data as $member)
            $res[$member->email] = $member->fullName;

        echo json_encode($res);
    }
}