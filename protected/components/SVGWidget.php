<?php
Class SVGWidget Extends CWidget {

	public $scale = 100;
	public $htmlOptions = array();

	public function init()
	{
		$this->getDomDocument(True); // This is a singleton class. For each initialization, we have to clean up domDocument and xpath in order to stop messing with widgets when they are called multiple times in a page
		$this->getXPath(True);
	}

	public function getSource() {
		return $this->source;
	}

	public function setSource($source) {
		$this->source = $source;
	}

	public function getDomDocument($force = False) {
		static $document;

		if($document && !$force)
			return $document;

		libxml_use_internal_errors(True);
		$document = new DomDocument;
		$document->loadXML(file_get_contents($this->source));

		return $document;
	}

	public function getXPath($force = False) {
		static $xpath;

		if($xpath && !$force)
			return $xpath;

		$xpath  = New DOMXPath( $this->domDocument );
		return $xpath;
	}

	public function getOutputPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.Yii::app()->languages->currentLanguage.'/'.$this->scale.'.png';
	}

	public function getOutputDirectory()
	{
		$outputDirectory = $this->outputPath;
		$outputDirectory = explode('/', $outputDirectory);
		array_pop($outputDirectory);
		return implode('/', $outputDirectory);
	}

	protected function getTempPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.Yii::app()->languages->currentLanguage.'.svg';
	}

	public function run() {
		print CHtml::image($this->outputUrl, '', array_merge(array('id' => $this->id), $this->htmlOptions));
	}

	public function save() {
		if(file_exists($this->outputPath))
			return ;

		$this->generate();
		$this->scale();

		if(!file_exists($this->outputDirectory))
			mkdir($this->outputDirectory, 0777, True);

		$svg = fopen($this->tempPath, 'w+');
		fwrite($svg, $this->domDocument->saveXML());
		fclose($svg);

		exec('rsvg '.$this->tempPath.' '.$this->outputPath);
	}

	public function getOutputUrl() {
		$this->save();
		return Yii::app()->assetManager->publish($this->outputPath);
	}

	public function setScale($scale)
	{
		$this->scale = $scale;
	}

	public function getScale()
	{
		return (int)$this->scale;
	}

	public function scale()
	{
		$root = $this->xpath->query('/svg')->item(0);
		$level1s = $this->xpath->query('/svg/*');

		$g = New DOMelement('g');
		$root->appendChild($g);
		$g->setAttribute('transform', 'scale('.($this->scale/100).')');

		foreach($level1s as $level1) {
			$g->appendChild($level1->cloneNode(True));
			$root->removeChild($level1);
		}

		$root->setAttribute('width', $root->getAttribute('width') * ($this->scale/100));
		$root->setAttribute('height', $root->getAttribute('height') * ($this->scale/100));
	}

	public function clearCache()
	{
		if(!file_exists($this->outputDirectory))
			return ;

		$dir = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->outputDirectory), RecursiveIteratorIterator::CHILD_FIRST);
		for ($dir->rewind(); $dir->valid(); $dir->next()) {
			if ($dir->isDir()) {
				@rmdir($dir->getPathname());
			} else {
				unlink($dir->getPathname());
			}
		}
	}
}