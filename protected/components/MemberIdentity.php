<?php
Class MemberIdentity Extends CBaseUserIdentity {
	protected $_id;
	protected $_email;
	protected $_password;

	public function __construct($email, $password) {
		$this->email = $email;
		$this->password = $password;
	}

	public function authenticate() {
		$member = Member::model()->findByAttributes(array('email' => $this->email));

		if($member)
			if($member->password == md5($this->password))
				$this->errorCode = self::ERROR_NONE;
			else
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode = self::ERROR_USERNAME_INVALID;

		if($member && $this->errorCode == self::ERROR_NONE)
			$this->id = $member->id;

		return $this->errorCode == ERROR_NONE;
	}

	public function getId() {
		return $this->_id;
	}

	public function setId($id) {
		$this->_id = $id;
	}

	public function getEmail() {
		return $this->_email;
	}

	public function setEmail($email) {
		$this->_email = $email;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function setPassword($password) {
		$this->_password = $password;
	}

	public function getName() {
		return Member::model()->findByPK($this->id)->fullName;
	}
}
?>