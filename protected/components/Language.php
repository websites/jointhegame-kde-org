<?php
Class Language Extends CComponent {
	protected $_languages;

	public function init() {
		$all_languages = parse_ini_file(Yii::getPathOfAlias('application.components').'/all_languages.ini', True, INI_SCANNER_RAW);
		$langs_dir= opendir(__DIR__.'/../../langs/');

		$available_langs = Array(
			'en' => 'English'
		);
		while($lang = readdir($langs_dir)) {
			if($lang{0} === '.')
				continue;

			if($lang === 'x-test')
				continue;

			$available_langs[$lang] = $all_languages[$lang]['Name'][$lang];
		}
		asort($available_langs);

		$this->_languages = $available_langs;
	}

	public function getLanguages() {
		return $this->_languages;
	}

	public function getLanguageTitle($language) {
		return $this->_languages[$language];
	}

	public function getCurrentLanguage() {
		if($current = $_GET['language'])
			if ($this->_languages[$current]) //Make sure its valid.
				return $current;

		if($current = $_COOKIE['language'])
			if($this->_languages[$current])
				return $current;


		$langs = array();
		if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			// break up string into pieces (languages and q factors)
			preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

			if (count($lang_parse[1])) {
				// create a list like "en" => 0.8
				$langs = array_combine($lang_parse[1], $lang_parse[4]);

				// set default to 1 for any without q factor
				foreach ($langs as $lang => $val) {
					if ($val === '') $langs[$lang] = 1;
				}

				// sort list based on value
				arsort($langs, SORT_NUMERIC);
			}
		}

		$available_langs = array_keys($this->_languages);

		foreach(array_keys($langs) as $lang) {
			$lang = str_replace('-', '_', $lang);

			foreach($available_langs as $available_lang)
				if(strtolower($lang) === strtolower($available_lang))
					return $available_lang;
		}

		return Yii::app()->language; //Return default lang.
	}
}