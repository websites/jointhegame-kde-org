<?php
Class Controller Extends CController {
	public $menu = array();

	public $teaser = True;
	public $title;
	public $extra = False;
	public $showJoinStuff = False;

	public function filters() {
		return array(
			'accessControl',
			'verification'
		);
	}

	public function filterVerification($filterChain) {
		if(Yii::app()->user->isGuest)
			return $filterChain->run();

		$controller = Yii::app()->controller;
		if($controller InstanceOf JoinController)
			if($controller->action->id == 'verify' || $controller->action->id == 'verification')
				return $filterChain->run();

		$member = Member::model()->findByPk(Yii::app()->user->id);
		if($member->verification)
			return $this->redirect(array('/join/verification'));
		else
			$filterChain->run();
	}

	public function init() {
		Yii::app()->language = Yii::app()->languages->currentLanguage;
		$menu['/'] = Yii::t('Menu', 'Home');

		if(Yii::app()->user->isGuest)
			$menu['/join/start'] = Yii::t('Menu', 'Join');
		else {
			$menu['/member/widgets'] = Yii::t('Menu', 'Widgets');
			$menu['/member/profile'] = Yii::t('Menu', 'Edit profile');
			$menu['/member/payments'] = Yii::t('Menu', 'My donations');
		}

		$menu['/member'] = Yii::t('Menu', 'List of members');
		$menu['mailto:jtg@kde.org'] = Yii::t('Menu', 'Contact us');

		if(Yii::app()->user->isGuest)
			$menu['/member/login'] = Yii::t('Menu', 'Login');
		else {
			$menu['/member/logout'] = Yii::t('Menu', 'Logout');

			$member = Member::model()->findByPk(Yii::app()->user->id);

			if($member->isAdmin) {
				$menu['/member/admin'] = Yii::t('Menu', 'Members');
				$menu['/payment/admin'] = Yii::t('Menu', 'Payments');
			}
		}

		$this->menu = $menu;
	}

	public function addMessage($message) {
		$this->extra .= "<div class='message'>{$message}</div>";
	}

	public function beforeRender($view) {
		//We dont want to insist him to join, when he is already joined (logged in)
		if(Yii::app()->user->isGuest)
			$this->showJoinStuff = True;

		return parent::beforeRender($view);
	}

	public function beforeAction($a) {
		return parent::beforeAction($a);
	}
}
?>
