<?php
include_once('BannerWidget.php');

Class FullBannerWidget Extends BannerWidget {
	public function getSource() {
		return YiiBase::getPathOfAlias('application.svg').'/full.svg';
	}

	public function generate() {
		$mains = $this->xpath->query("//tspan[@id='main']");

		foreach($mains as  $main) {
			$main->nodeValue = Yii::t('member', ':fullName, KDE supporting member.', Array(
				':fullName' => $this->member->fullName
			));
		}

		$date = $this->member->firstPayment->date;
		$alts = $this->xpath->query("//tspan[@id='alt']");
		foreach($alts as  $alt) {
			$alt->nodeValue = Yii::t('member', 'Since :date.', Array(
				':date' => date('F Y', $date)
			));
		}
	}
}