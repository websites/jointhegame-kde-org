<?php
Class FullBannerWidget Extends SVGWidget {
	public function setMember(Member $member) {
		$this->member = $member;
	}

	public function getMember() {
		return $this->member;
	}

	public function getSource() {
		return YiiBase::getPathOfAlias('application.svg').'/full.svg';
	}

	public function generate() {
		$mains = $this->xpath->query("//text[@id='mains']");

		foreach($mains as  $main) {
			$main->nodeValue = $this->member->first_name.' '.$this->member->last_name;
		}
	}

	public function getOutputPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.$this->member->id.'/'.$this->scale.'.png';
	}

	protected function getTempPath() {
		$name = get_class($this);
		return YiiBase::getPathOfAlias('application.runtime').'/'.$name.'/'.$this->member->id.'.svg';
	}
}