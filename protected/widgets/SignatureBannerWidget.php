<?php
include_once('BannerWidget.php');

Class SignatureBannerWidget Extends BannerWidget {
	public function getSource() {
		return YiiBase::getPathOfAlias('application.svg').'/signature.svg';
	}

	public function generate() {
		$mains = $this->xpath->query("//tspan[@id='name']");

		foreach($mains as  $main) {
			$main->nodeValue = Yii::t('member', ':fullName, KDE supporting member.', Array(
				':fullName' => $this->member->fullName
			));
		}
	}
}