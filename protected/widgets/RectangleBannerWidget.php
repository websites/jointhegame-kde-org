<?php
include_once('BannerWidget.php');

Class RectangleBannerWidget Extends BannerWidget {
	public function getSource() {
		return YiiBase::getPathOfAlias('application.svg').'/rectangle.svg';
	}

	public function generate() {
		$names = $this->xpath->query("//tspan[@id='name']");
		foreach($names as  $name)
			$name->nodeValue = Yii::t('member', ':fullName,', Array(
				':fullName' => $this->member->fullName
			));

		$texts = $this->xpath->query("//tspan[@id='text']");
		foreach($texts as  $text)
			$text->nodeValue = Yii::t('member', 'KDE Supporting member.');

		$date = $this->member->firstPayment->date;
		$alts = $this->xpath->query("//tspan[@id='date']");
		foreach($alts as  $alt)
			$alt->nodeValue = Yii::t('member', 'Since :date.', Array(
				':date' => date('F Y', $date)
			));
	}
}